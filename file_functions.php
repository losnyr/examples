<?php
/*
delete_dir - Function was used to safely delete directories and sub-directories for a series of flash training videos when a new version was uploaded.

Input:
$src - the full path of the video folder being deleted
$approved_root - redundant variable, initially placed to be able to expand the function for other uses, but ultimately the checks included a hard coded root set to the training video folder to ensure the function could not be misused to delete other important folders since it was only used for training videos

Output:
$status - an array with properties showing if the function succeeded or not and a message to display on success or the specific failure if it did not.
*/
function delete_dir($src, $approved_root) { 
	if(isset($approved_root) && !empty($approved_root) && isset($src) && !empty($src) && strpos($src, "/training/video/") !== FALSE){
		$approved_directories = scandir($approved_root);
		$directory_found = 0;
		foreach($approved_directories as $directory){
			if(strpos($src, $directory)===false){
				//Not found
			}else{
				$directory_found=1;
			}
		}
		if($directory_found==1){
			$dir = opendir($src);
			while(false !== ( $file = readdir($dir)) ) {
				if (( $file != '.' ) && ( $file != '..' )) {
					if ( is_dir($src . '/' . $file) ) {
						delete_dir($src . '/' . $file, $src);
					}else {
						unlink($src . '/' . $file);
					}
				}
			}
			rmdir($src);
			closedir($dir);
			$status = array("succeeded"=>true,"message"=>"Item successfully deleted.");
		}else{
			$status = array("succeeded"=>false,"message"=>"You do not have access to delete this.");
		}
	}else{
		$status = array("succeeded"=>false,"message"=>"The system could not determine if you have access to delete this.");
	}
	return $status;
}


?>