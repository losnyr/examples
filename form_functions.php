<?php
/*class.sqlreportbuilder.php is a prebuilt class used to export formatted reports to various file types*/

require($_SERVER['DOCUMENT_ROOT']."/_lib/class.sqlreportbuilder.php");?><?php 

//merge_field_properties - Used to add a basic set of properties to any fields involved in the table/view being dealt with and merges with the original properties passed to the function.  Field types that are manually set prior to being sent to the function will be preserved.

/*Input:
$attributes['field_list'] - passes in an array of field names from the appropriate table or view
$attributes['table'] - The table or view involved in any SQL statements being built

Output:
$fields - Array of all fields merged with the appropriate properties assigned
*/
function merge_field_properties($attributes){
	global $pdo;
	global $DBname;
	$fields = array();
	$parameter_array = array(':DBname' => $DBname,
							':table'	=> $attributes['table']);
	$field_names = "AND COLUMN_NAME in(";
	$count = 0;
	foreach($attributes['field_list'] as $k => $field){
		$field_names .= ":$count,";
		$parameter_array[":$count"] = $k;
		$count++;
	}
	$field_names = rtrim($field_names, ",").")";
	//Selects basic attributes of the fields directly from the information_schema entry for the specified table or view to later determine how to format the specific fields in reports as well as the proper method for how to format any sql statements involving the fields
	$sql = "SELECT COLUMN_NAME as field_name, COLUMN_DEFAULT as field_default, IS_NULLABLE as field_nullable, DATA_TYPE as field_type, CHARACTER_MAXIMUM_LENGTH as max_length, NUMERIC_PRECISION as numeric_length, NUMERIC_SCALE as decimal_points
				FROM information_schema.COLUMNS
				WHERE TABLE_SCHEMA=:DBname 
				AND TABLE_NAME=:table
				$field_names";
	$stmt = $pdo->prepare($sql);
	$stmt->execute($parameter_array);
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Stores and indexes the fields by field name
	$db_list = array();
	foreach($result as $k => $valArray){
		$db_list[$valArray['field_name']] = $valArray;
	}
	//Loops through the passed list of fields and matches them with the results found from the above SQL statement
	foreach($attributes['field_list'] as $k => $field){
		if(isset($db_list[$k])){
			$fields[$k] = array_merge($field, $db_list[$k]);
		}else{
			$fields[$k] = $field;
		}
		//Overrides the field_type property if it was already set manually prior to being passed
		if(isset($attributes['field_list'][$k]['field_type'])){
			$fields[$k]['field_type'] = $attributes['field_list'][$k]['field_type'];
		}
	}
	return $fields;
}

//Passes in the desired fields being included with the appropriate characteristics assigned from the merge_field_properties function and/or manually set as desired.
/*
Input:
$attributes['fields'] - Array of fields and their set properties
$attributes ['form_type'] - Determines what function the form will fulfill, currently only search is completed and tested.

Output:
$sql - The SQL statement to be run.  If being used in a paginated report, it includes a limit to reduce the resources necessary to run.
$count_sql - Can be run to determine the total number of records returned by running the statement returned in the $sql variable
$export_sql - A full statement that can be run to be used in exporting a full set of data in the case that the statement returned in the $sql variable is eing limited
$parameter_array - A list of parameters to be used in running the SQL statement in PDO
$errors - Any errors encountered in running the function to be displayed
*/
function build_form_sql($attributes){
	$fields = $attributes['fields'];
//	pre_r($attributes);
	$errors = array();
	switch($attributes['form_type']){
		//Used for building select statements for displaying data and creating reports
		case "search":			
			$parameter_array = array();
			$sql = "SELECT ";
			$count_sql = "";
			foreach($fields as $k=> $field){
				//hide_results attribute for a field will prevent it from being included in the shown fields
				if(!isset($field['hide_results']) || $field['hide_results'] != true){
					$field_name = $k;
					//Checks if the field's name includes a period or space and encapsulates it to prevent any issues in running the statement
					if(strpos($field_name, ".") > -1 && strpos($field_name, " ") > -1){
						$field_name = str_replace(".",".`", $field_name);
					}elseif(strpos($field_name, " ") > -1){
						$field_name = "`$field_name`";
					}
					//Checks if the field has a specific label being assigned to it and sets an alias in the SQL statement
					if(isset($field['field_label']) && (!isset($field['field_type']) || $field['field_type'] != "html")){
						if($field['field_type'] == 'aggregate'){
							$sql.= $field['aggregation']." as `$k`,";
						}else{
							$sql.= "$field_name as `".$field['field_label']."`,";
						}
					}
				}
			}
			$sql = rtrim($sql, ",") . " FROM ".$attributes['table'];
			$count_sql = "SELECT count(*) AS record_count FROM ".$attributes['table'];
			
			$search_field_count = 0;
			$having_field_count = 0;
			$group_count = 0;
			$having_fields = " HAVING ";
			$search_fields = " WHERE ";
			$group_by = " GROUP BY ";
			//Loops through the fields and filters the SQL statement based on the submitted criteria
			foreach($fields as $k => $field){
				$field_name = $k;
				if(strpos($field_name, ".") > -1 && strpos($field_name, " ") > -1){
					$field_name = str_replace(".",".`", $field_name);
				}elseif(strpos($field_name, " ") > -1){
					$field_name = "`$field_name`";
				}
				if(isset($field['group_by']) && $field['group_by'] == TRUE){
					$group_by .= "$field_name, ";
					$group_count++;
				}
				$generated_fields = array("max_records","record_start");
				if(!in_array($k, $generated_fields)){
					//Checks for any spaces and replaces with underscores
					$parameter_name = str_replace(".","",str_replace(" ", "_",$k));
					//Filters out standard post variables that should not be included in the SQL statement.  Also checks if the fields require a range of values or bitwise operator
					if($k != "form_type" && $k != "table" && $k != "data_changed"){
						if(isset($field['value']) && is_array($field['value'])){
							if(@$field['range'] === true){
								if(isset($field['value'][0]) && $field['value'][0] != ""){
									$search_fields.= "$field_name >= :$parameter_name"."0 AND ";
									$parameter_array[":$parameter_name"."0"]=format_input($field['value'][0], $field);
									$search_field_count++;
								}
								if(isset($field['value'][1]) && $field['value'][1] != ""){
									$search_fields.= "$field_name <= :$parameter_name"."1 AND ";
									$parameter_array[":$parameter_name"."1"]=format_input($field['value'][1], $field);
									$search_field_count++;
								}
							}else{
								$multiselect_parameters="(";
									foreach($field['value'] as $i => $field_value){
										if($fields[$k]['field_type'] == "bit_multiselect"){
											$multiselect_parameters.="$field_name & :$parameter_name$i OR ";
										}else{
											$multiselect_parameters.="$field_name = :$parameter_name$i OR ";
										}
										$parameter_array[":$parameter_name$i"]=$field_value;
									}
								$search_field_count++;
								$search_fields.=rtrim($multiselect_parameters," OR "). ") AND ";
							}
						}else{
							if(isset($field['value']) && $field['value'] != ""){
								//Checks for partial searches by looking for % in the value
								if(strpos($field['value'], "%") === false){
									if(strpos($field_name, "start_") !== false){
										if($field['field_type'] == "aggregate"){
											$having_fields .= str_replace("start_","",$field_name)." >= :$parameter_name AND ";
										}else{
											$search_fields.= str_replace("start_","",$field_name)." >= :$parameter_name AND ";
										}
									}elseif(strpos($k, "end_") !== false){
										if($field['field_type'] == "aggregate"){
											$having_fields .= str_replace("end_","",$field_name)."` >= :$parameter_name AND ";
										}else{
											$search_fields.= str_replace("end_","",$field_name)."` >= :$parameter_name AND ";
										}
									}else{
										if($field['field_type'] == "aggregate"){
											$having_fields .= "$field_name = :$parameter_name AND ";
										}else{
											$search_fields.= "$field_name = :$parameter_name AND ";
										}
										
									}
								}else{
									$search_fields .= "$field_name like :$parameter_name AND ";
								}
								$parameter_array[":$parameter_name"] = $field['value'];
								if($field['field_type'] == "aggregate"){
									$having_field_count++;
								}else{
									$search_field_count++;
								}
							}
						}
					}
				}
			}
			
			if($search_field_count > 0){
				$count_sql .= rtrim($search_fields, " AND ");
				$sql .= rtrim($search_fields," AND ");
			}
			if($group_count > 0){
				$count_sql .= rtrim($group_by, ", ");
				$sql .= rtrim($group_by, ", ");
			}
			if($having_field_count > 0){
				$count_sql .= rtrim($having_fields, " AND ");
				$sql .= rtrim($having_fields, " AND ");
			}
			
			//Sets grouping based on any parameters assigned
			$order_by = " ORDER BY ";
			$order_count = 0;
			if(isset($attributes['grouping']) && is_array($attributes['grouping']) && count($attributes['grouping']) > 0){
				foreach($attributes['grouping'] as $group){
					if(strpos($_POST['order_by'], ".") > -1 && strpos($_POST['order_by'], " ") > -1){
						$order_by .= $group.", ";
						$order_count++;
					}elseif(strpos($group, " ") > -1){
						$order_by .= "`".$group."`, ";
						$order_count++;
					}else{
						$order_by .= $group.", ";
						$order_count++;
					}
				}
			}
			//Sets any ordering based on post variables
			if(isset($_POST['order_by']) && $_POST['order_by'] != ""){
				if(strpos($_POST['order_by'], ".") > -1 && strpos($_POST['order_by'], " ") > -1){
					$order_by .= str_replace(".",".`", $_POST['order_by']).", ";
					$order_count++;
				}elseif(strpos($_POST['order_by'], " ") > -1){
					$order_by .= "`".$_POST['order_by']."`, ";
					$order_count++;
				}else{
					$order_by .= $_POST['order_by'].", ";
					$order_count++;
				}
			}
			//Adds sorting directions based on post variables
			if($order_count > 0){
				$count_sql .= rtrim($order_by, ", ");
				$sql .= rtrim($order_by, ", ");
				if(isset($_POST['order_dir']) && $_POST['order_dir'] != ""){
					$count_sql .= " ".$_POST['order_dir'];
					$sql .= " ".$_POST['order_dir'];
				}else{
					$count_sql .= " ASC";
					$sql .= " ASC";
				}
			}
			//Sets export_sql to the original sql statement before any limits are added
			$export_sql = $sql;
			//$count_sql = $sql;
			if(isset($_POST['record_start']) && isset($_POST['max_records'])){
				if($_POST['max_records'] != "all"){
					$sql.= " LIMIT ".$_POST['record_start'].", ".$_POST['max_records'];
				}
			}
		
		break;
		//Add function not completed/tested.  Was commented out to prevent being used
		case "add":
			/*
			$parameter_array = array();
			$sql = "INSERT INTO ".$form_post['table']." (";
			$insert_field_count = 0;
			foreach($form_post as $k => $post){
				if($k != "form_type" && $k != "table" && $k != "data_changed"){
					$sql.= $k.", ";
				}
			}
			$sql = rtrim($sql,", ").") VALUES (";
			foreach($form_post as $k => $post){
				if($k != "form_type" && $k != "table" && $k != "data_changed"){
					if($fields[$k]['field_nullable'] == "NO" && $post == ""){
						if($fields[$k]['field_type'] == "enum"){
							$errors[]="Select ".a_an($fields[$k]['field_label'])." ".$fields[$k]['field_label'];
						}else{
							$errors[]="Enter ".a_an($fields[$k]['field_label'])." ".$fields[$k]['field_label'];
						}
					}
					if(isset($fields[$k]['min']) && $post < $fields[$k]['min'] && $post != ""){
						$errors[]=$fields[$k]['field_label']." must be greater than ".$fields[$k]['min'];
					}
					if(isset($fields[$k]['max']) && $post > $fields[$k]['max'] && $post != ""){
						$errors[]=$fields[$k]['field_label']." must be less than ".$fields[$k]['max'];
					}
					switch($fields[$k]['field_type']){
						case "date":
							$parameter_array[":$k"] = dateToDb($post);
						break;
						case "time":
							$parameter_array[":$k"] = date("H:i:s", strtotime($post));
						break;
						case "datetime":
						case "timestamp":
							$parameter_array[":$k"] = date("m-d-y H:i:s A", strtotime($post));
						break;
						case "float":
						case "double":
						case "decimal":
						case "smallint":
						case "mediumint":
						case "bigint":
						case "int":
							$numeric_length = strlen($post);
							if($numeric_length > $fields[$k]['numeric_length']){
								$errors[]=$fields[$k]['field_label']." can only be ".$fields[$k]['numeric_length']." total digits long";
							}
							$number_array = explode(".",$post);
							if(isset($number_array[1])){
								$decmial_length = strlen($number_array[1]);
								if($decmial_length > $fields[$k]['decimal_points']){
									$errors[]=$fields[$k]['field_label']." can only have ".$fields[$k]['decmial_points']." digits past the decimal";
								}
							}
						break;
						default:
							if(strlen($post) > $fields[$k]['max_length']){
								$errors[]=$fields[$k]['field_label']." has a maximum of ".$fields[$k]['max_length']." characters";
							}else{
								$parameter_array[":$k"] = $post;	
							}
						break;
					}
					$sql .= ":$k, ";
					$insert_field_count++;
				}
			}
			$sql = rtrim($sql,", ");
			*/
		break;
	}
	return array("sql"=>$sql, "count_sql"=>@$count_sql, "export_sql"=>@$export_sql, "parameters"=>$parameter_array, "errors"=>$errors);
}

//Builds a form with any appropriate inputs based on the field types determined by merge_field_properties and/or manually set on the originating script
/*
Input:
$attributes['fields'] - array of fields and their properties
$attributes['form_description'] - Text to display above the form
$attributes['form_title'] - Form's title to be displayed in a legend tag
$attributes['action'] - Form's action property, typically set to the originating script
$attributes['form_name'] - Unique name for the form on the page
$attributes['form_class'] - Any class properties necssary for the form for styling purposes
*/
function build_form($attributes){
	$fields = $attributes['fields'];
	?>
	<fieldset>
    <?=isset($attributes['form_description']) && $attributes['form_description'] != "" ? "<p>".$attributes['form_description']."</p>" : ""?>
	<legend><?=$attributes['form_title']?></legend>
    <form action="<?=$attributes['action']?>" id="<?=$attributes['form_name']?>" method="post" class="<?=$attributes['form_class']?>">
    <input name="form_type" type="hidden" value="<?=$attributes['form_type']?>">
    <input name="order_by" id="order_by" type="hidden" value="<?=@$_POST['order_by']?>">
    <input name="order_dir" id="order_dir" type="hidden" value="<?=@$_POST['order_dir']?>">
    <?php
	foreach($fields as $field){
		//Checks if the field is included in the searchable fields and skips any unnecessary steps
		if(!@$field['hide_search']){
			$class = "";
			if(isset($field['class'])){
				$class = $field['class'];
			}
			//Checks if the field is required or not and adds the isRequired class to force users to include a value in order to submit
			if(@$field['field_nullable'] == "NO" && (@$attributes['form_type'] == "add" || @$attributes['form_type'] == "edit")){
				$class = trim($class." isRequired");
			}
			//Switch determines the appropriate input type on the form based on the assigned field_type
			switch($field['field_type']){
				//Numeric values.  Can be set to a range of values and have minimum/maximum values assigned.
				case "float":
				case "double":
				case "decimal":
				case "smallint":
				case "mediumint":
				case "bigint":
				case "int":
					?>
					<p>
					<label><?=@$field['field_label']?></label>
					<input name="<?=@$field['field_name']?><?=@$field['range']===true ? "[]" : ""?>" value="<?=@$field['range'] === true ? $field['value'][0] : @$field['value']?>" class="isNumeric <?=$class?>"<?=isset($field['min']) && is_numeric($field['min']) ? " min='".$field['min']."'" : ""?><?=isset($field['max']) && is_numeric($field['max']) ? " max='".$field['max']."'" : ""?><?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
					<?php
					if(@$field['range']===true){
					?> through
					<input name="<?=@$field['field_name']?>[]" value="<?=@$field['value'][1]?>" class="isNumeric <?=$class?>"<?=isset($field['min']) && is_numeric($field['min']) ? " min='".$field['min']."'" : ""?><?=isset($field['max']) && is_numeric($field['max']) ? " max='".$field['max']."'" : ""?><?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
                    (leave blank for no upper range)
					<?php }?>
					</p>
					<?php
				break;
				//Used for true/false select boxes
				case "bit":
				case "tinyint":
					?>
					<p>
					<label><?=@$field['field_label']?></label>
					<select name="<?=@$field['field_name']?>" class="<?=$class?>"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
						<option value=""></option>
						<option value="0"<?=@$field['value']===0 ? " selected" : ""?>>No</option>
						<option value="1"<?=@$field['value']==1 ? " selected" : ""?>>Yes</option>
					</select>
					</p>
					<?php
				break;
				//Smaller text values, only requires a standard input box
				case "char":
				case "tinytext":
				case "varchar":
				case "aggregate":
					?>
					<p>
					<label><?=@$field['field_label']?></label>
					<input name="<?=@$field['field_name']?>" value="<?=@$field['value']?>" class="<?=$class?>"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
					</p>
					<?php
				break;
				//Builds a select box with the assigned values to the enum field
				case "enum":
					?>
					<p>
					<label><?=@$field['field_label']?></label>
					<select name="<?=@$field['field_name']?>" class="<?=$class?>"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
						<option value=""></option>
						<?php
						$option_array = get_enum_as_array($GLOBALS['DBname'], $attributes['table'], @$field['field_name']);
						foreach($option_array as $option){
						?>
						<option value="<?=$option?>"<?=$option==@$field['value'] ? " selected" : ""?>><?=$option?></option>
						<?php	
						}
						?>
					</select>
					</p>
					<?php
				break;
				//Creates an input box with the calTimepicker class for use with a jQuery library to create a formatted time picker input
				case "time":
					?>
					<p>
					<label><?=@$field['field_label']?></label>
					<input type="text" name="<?=@$field['field_name']?><?=@$field['range']===true ? "[]" : ""?>" value="<?=@$field['range'] === true ? $field['value'][0] : @$field['value']?>" id="<?=@$field['range']===true ? "start_" : ""?><?=@$field['field_name']?>"  class="calTimepicker <?=$class?>" size="10" maxlength="10"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
					<?php
					if(@$field['range']===true){
					?> -
					<input type="text" name="<?=@$field['field_name']?>[]" value="<?=@$field['value'][1]?>" id="end_<?=@$field['field_name']?>"  class="calTimepicker <?=$class?>" size="10" maxlength="10"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
					<?php }?>
					</p>
					<?php
				break;
				//Creates an input box with the calDatepicker class for use with a jQuery library to create a formatted date picker input
				case "date":
					?>
					<p>
					<label><?=@$field['field_label']?></label>
					<input type="text" name="<?=@$field['field_name']?><?=@$field['range']===true ? "[]" : ""?>" value="<?=@$field['range'] === true ? $field['value'][0] : @$field['value']?>" id="<?=@$field['field_name']?>"  class="calDatepicker <?=$class?>" size="10" maxlength="10"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
					<?php
					if(@$field['range']===true){
					?> -
					<input type="text" name="<?=@$field['field_name']?>[]" value="<?=@$field['value'][1]?>" id="end_<?=@$field['field_name']?>"  class="calDatepicker <?=$class?>" size="10" maxlength="10"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
					<?php }?>
					</p>
					<?php
				break;
				//Creates an input box with the calDateTimepicker class for use with a jQuery library to create a formatted date/time picker input
				case "datetime":
				case "timestamp":
					?>
					<p>
					<label><?=@$field['field_label']?></label>
					<input type="text" name="<?=@$field['field_name']?><?=@$field['range']===true ? "[]" : ""?>" value="<?=@$field['range'] === true ? $field['value'][0] : @$field['value']?>" id="<?=@$field['range']===true ? "start_" : ""?><?=@$field['field_name']?>" class="calDateTimepicker <?=$class?>" size="10" maxlength="10"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
					<?php
					if(@$field['range']===true){
					?> -
					<input type="text" name="<?=@$field['field_name']?>[]" value="<?=@$field['value'][1]?>" id="end_<?=@$field['field_name']?>" class="calDateTimepicker <?=$class?>" size="10" maxlength="10"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
					<?php }?>
					</p>
					<?php
				break;
				//Larger text values, creates a textarea
				case "longtext":
				case "mediumtext":
				case "text":
					?>
					<p>
					<label><?=@$field['field_label']?></label>
					<textarea name="<?=@$field['field_name']?>" id="<?=@$field['field_name']?>" class="<?=$class?>"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?>><?=@$field['value']?></textarea>
					</p>
					<?php
				break;
				//Manual field type, used to create a dropdown that allows multiple values to be checked
				case "multiselect":
					?>
					<p>
					<label><?=@$field['field_label']?></label>
					<select name="<?=@$field['field_name']?>[]" class="<?=$class?> multiselect" multiple="multiple"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
						<?php
						foreach($field['multiselect_values'] as $value => $label){
							$selected = "";
							if(is_array($field['value']) && count($field['value']) > 0){
								foreach($field['value'] as $set_value){
									if($value == $set_value){
										$selected = "selected";
									}
								}
							}
						?>
						<option value="<?=$value?>" <?=$selected?>><?=$label?></option>
						<?php
						}
						?>
					</select>
					</p>
					<?php
				break;
				//Manual field type.  Used to create a dropdown with text labels and binary values that allows multiple values to be checked
				case "bit_multiselect":
					?>
					<p>
					<label><?=@$field['field_label']?></label>
					<select name="<?=@$field['field_name']?>[]" class="<?=$class?> multiselect" multiple="multiple"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?>>
						<?php
						foreach($field['multiselect_values'] as $value => $label){
							$selected = "";
							if(is_array($field['value']) && count($field['value']) > 0){
								foreach($field['value'] as $set_value){
									if($value & $set_value){
										$selected = "selected";
									}
								}
							}
						?>
						<option value="<?=$value?>" <?=$selected?>><?=$label?></option>
						<?php
						}
						?>
					</select>
					</p>
					<?php
				break;
				//Manual field type.  Used to create specialized breaks inbetween different field inputs.  Merely echos the assigned value.
				case "html":
					echo @$field['html'];
				break;
			}
		}/*else{
			?>
            <input name="<?=@$field['field_name']?>" value="<?=@$field['value']?>" class="<?=$class?>"<?=isset($field['style']) ? "style='".$field['style']."'" : ""?> type="hidden">
            <?php
		}*/
	}
	//Creates a reset button that allows any changed values to be set back to the original values
	if(isset($attributes['allow_reset']) && $attributes['allow_reset']){
	?>
    <input type="reset" class="reset" value="Reset" />
    <?php	
	}
	?>
    <input type="submit" class="submit" value="<?=$attributes['submit_label']?>">
    <?php
	//If the report is desired to be paginated, creates an area to submit the starting point and number of records to show
	if(isset($attributes['paginate']) && $attributes['paginate'] == true){
	?>
    <input type="hidden" id="record_start" name="record_start" value="<?=isset($_POST['record_start']) && $_POST['record_start'] != "" ? $_POST['record_start'] : 0?>" />
    <input type="hidden" id="max_records" name="max_records" value="<?=isset($_POST['max_records']) ? $_POST['max_records'] : 50?>" />
    <?php
	}
	?>
    </form>
    </fieldset>
    <?php
}

//list_records - Creates an html table to show the results of a select statement created by the build_form_sql function
/*
Input:
$attributes['count_sql'] - SQL statement returned from build_form_sql to determine the total number of records if it were run without limits
$attributes['paginate'] - Determines if the results are being paginated and runs the count SQL statement (otherwise a count of records is determined from the statement returning the results)
$attributes['sql'] - SQL statement to be run with any limits
$attributes['parameters'] - List of parameters used in the SQL statmement
$attributes['export_csv'] - Determines whether to show a link for exporting the results to CSV format
$attributes['chart_options'] - Used for showing links to display charts for the dataset
*/
function list_records($attributes){
//	pre_r($attributes);
	global $pdo;
	//Determines if all records are to be shown or a partial results set based on pagination
	$total_results = "all";
	if(@$_POST['max_records'] != "all"){
		if(isset($attributes['count_sql']) && $attributes['count_sql'] != "" && $attributes['paginate'] == true){
			$stmt = $pdo->prepare($attributes['count_sql']);
			$stmt->execute($attributes['parameters']);
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			echo "<input type='hidden' name='strpos' value='".strpos($attributes['count_sql'], "GROUP BY")."'>";
			if(count($result) > 0){
				if(count($result) > 1){
					$total_results = count($result);
				}else{
					$total_results = $result[0]['record_count'];
				}
			}else{
				$total_results = 0;
			}
		}
	}
	$stmt = $pdo->prepare($attributes['sql']);
	$query_status = $stmt->execute($attributes['parameters']);
//	echo "Error Code: ".$pdo->errorCode()."<br>";
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	if(@$_POST['max_records'] == "all"){
		$total_results = count($result);
	}
//	pre_r($result);
	?>
    <div id="results-area">
    <?php //Creates a download link for exporting the displayed data?>
	<?=$attributes['export_csv'] ? '<a class="downloadLink" rel="csv" formId="'.$attributes['form_name'].'" style="border:solid 2px #f37021;background:#eee url(/_img/icon/xls_file.png) 5% 50% no-repeat;display:inline-block;padding:5px 20px 5px 50px;line-height:25px;">Export to CSV</a>&nbsp;&nbsp;&nbsp;' : ""?>
    <table class="data_table" id="results"
    <?php
		//Used for displaying links for showing charts
    	if(isset($attributes['chart_options']) && is_array($attributes['chart_options'])){
			foreach($attributes['chart_options'] as $a => $chart_option){
				echo "$a='$chart_option' ";
			}
		}
	?>>
    <?php
	//Loops through results and shows data formatted based on data type
	if(count($result) >0){
		//Top row is the field labels.  Each cell includes a link to sort by the field if the sortable attribute is set to true.
	?>
    	<thead>
    	<tr>
        <?php
			$shown_field_count = 0;
			foreach($attributes['fields'] as $k => $field){
				if(isset($field['field_label']) && !@($field['hide_results'])){
					$shown_field_count++;
					if(isset($field['sortable']) && $field['sortable'] == true){
						$dir_icon = "";
						if(@$_POST['order_by'] == $k && @$_POST['order_dir'] == "ASC"){
							$dir_icon = '<img src="/tools/view-reports/reports/phprptimages/sortup.gif">';
						}elseif(@$_POST['order_by'] == $k && @$_POST['order_dir'] == "DESC"){
							$dir_icon = '<img src="/tools/view-reports/reports/phprptimages/sortdown.gif">';
						}
						$title = '<a class="order_title" rel="'.$k.'" order_dir="'.$_POST['order_dir'].'">'.$field['field_label'].$dir_icon.'</a>';
					}else{
						$title = $field['field_label'];
					}
				?>
				<th<?=str_replace(" ","_",$k) == @$attributes['chart_options']['data-attc-colDescription'] || str_replace(" ","_",$k) == @$attributes['chart_options']['data-attc-colValues'] ? " id='".str_replace(" ","_",$k)."'" : ""?>><?=$title?></th>
				<?php	
				}
			}
		?>
        </tr>
        </thead>
    <?php
	//Starts running totals for accumulated values
	$previous_group_value = array();
	$accumulated_values = array();
	$grand_accumulated_values = array();
	if(isset($attributes['sub_totals']) && is_array($attributes['sub_totals'])){
		foreach($attributes['sub_totals'] as $k => $sub_total){
			$accumulated_values[$k] = 0;
			$grand_accumulated_values[$k] = 0;
		}
	}
	$counter = 0;
	foreach($result as $valArray){
		//Checks grouping and displays a sub-total for the group if appropriate
		if(is_array($previous_group_value)){
			foreach($previous_group_value as $k => $group_value){
				if($group_value['value'] != $valArray[$k] && $counter > 0){
		?>
        <tr class="totals">
        <?php
					foreach($attributes['fields'] as $k => $field){
						if(isset($field['field_label']) && !@($field['hide_results'])){
					
			?>
            <td>
			<?php
							if(in_array($k, $attributes['grouping'])){
								echo $group_value['value']." Total:";
							}elseif(isset($accumulated_values[$k])){
								echo number_format($accumulated_values[$k]);
							}
			?>
            </td>
            <?php	
						}
					}
			
				foreach($attributes['sub_totals'] as $k => $sub_total){
					$accumulated_values[$k] = 0;
				}
		?>
        </tr>
        <?php	
				}
			}
		}
		?>
        <tr>
        <?php
			foreach($attributes['fields'] as $k => $field){
				if(isset($field['field_label']) && !@($field['hide_results'])){
					if(isset($attributes['sub_totals']) && is_array($attributes['sub_totals'])){
						foreach($attributes['sub_totals'] as $j => $sub_total){
							if($k == $j){
								switch($attributes['sub_totals'][$k]){
									case "count":
										$accumulated_values[$k]++;
										$grand_accumulated_values[$k]++;
									break;
									case "sum":
										$accumulated_values[$k]+= $valArray[$k];
										$grand_accumulated_values[$k]+= $valArray[$k];
									break;
								}
							}
						}
					}
			?>
            <td>
			<?php
				$show_field = 1;
				if(isset($attributes['grouping'])){
					foreach($attributes['grouping'] as $group){
						if($k == $group && isset($previous_group_value[$k]) && $previous_group_value[$k]['value'] == $valArray[$k]){
							$show_field = 0;
						}
					}
				}
				//Displays the field as formatted by format_output function
        	    if($show_field){
					echo format_output($valArray[$field['field_label']], $field);
				}
			?>
            </td>
            <?php	
				}
			}
		?>
        </tr>
        <?php
		if(isset($attributes['grouping']) && is_array($attributes['grouping']) && count($attributes['grouping']) > 0){
			
			foreach($attributes['grouping'] as $group_name){
				$previous_group_value[$group_name]['value'] = $valArray[$group_name];
			}
		}	
		$counter++;	
	}
	
	if(isset($attributes['paginate']) && $attributes['paginate'] == true){
		
			$record_start = $attributes['form_post']['record_start'] + 1;
			$max_record = $attributes['form_post']['max_records'] == "all" ? $total_results : $attributes['form_post']['record_start'] + $attributes['form_post']['max_records'];
			$max_record = 
			$max_record = $max_record > $total_results ? $total_results : $max_record;

			$next_records = $attributes['form_post']['record_start'] + $attributes['form_post']['max_records'];
			
			$last_records = 0;
			if($attributes['form_post']['max_records'] > 0){
				$last_records = floor($total_results / $attributes['form_post']['max_records']) * $attributes['form_post']['max_records'];
			}
				
			$previous_records = $attributes['form_post']['record_start'] - $attributes['form_post']['max_records'];
			$previous_records = $previous_records < 0 ? 0 : $previous_records;
			
			$previous_results = $attributes['form_post']['record_start'] - $attributes['form_post']['max_records'];
			$next_results = $attributes['form_post']['record_start'] + $attributes['form_post']['max_records'];
			$last_results = $total_results - $attributes['form_post']['max_records'];
			
			$first_enabled = $attributes['form_post']['record_start'] == 0 || $attributes['form_post']['max_records'] == "all" ? 0 : 1;
			$previous_enabled = $previous_results < 0 || $attributes['form_post']['max_records'] == "all" ? 0 : 1;
			$next_enabled = $next_results > $total_results || $attributes['form_post']['max_records'] == "all" ? 0 : 1;
			$last_enabled = $attributes['form_post']['record_start'] >= $last_results || $attributes['form_post']['max_records'] == "all" ? 0 : 1;
			
			$total_pages = 1;
			$current_page = 1;
			if($attributes['form_post']['max_records'] != "all"){
				$total_pages = ceil($total_results / $attributes['form_post']['max_records']);
				$current_page = ceil($attributes['form_post']['record_start'] / $attributes['form_post']['max_records'])+1;
			}
			$current_page = $current_page > $total_pages ? $total_pages : $current_page;
			if(isset($attributes['sub_totals']) && is_array($attributes['sub_totals']) && count($attributes['sub_totals']) > 0){
		?>
        <tr class="totals">
        <?php
					foreach($attributes['fields'] as $k => $field){
						if(isset($field['field_label']) && !@($field['hide_results'])){
					
			?>
            <td>
			<?php
							if(in_array($k, $attributes['grouping'])){
								echo $group_value['value']." Total:";
							}elseif(isset($accumulated_values[$k])){
								echo number_format($accumulated_values[$k]);
							}
			?>
            </td>
            <?php	
						}
					}
			
				foreach($attributes['sub_totals'] as $k => $sub_total){
					$accumulated_values[$k] = 0;
				}
		?>
        </tr>
        <tr class="totals">
        <?php
					foreach($attributes['fields'] as $k => $field){
						if(isset($field['field_label']) && !@($field['hide_results'])){
					
			?>
            <td>
			<?php
							if(in_array($k, $attributes['grouping'])){
								echo "Grand Total:";
							}elseif(isset($grand_accumulated_values[$k])){
								echo number_format($grand_accumulated_values[$k]);
							}
			?>
            </td>
            <?php	
						}
					}
			
				foreach($attributes['sub_totals'] as $k => $sub_total){
					$accumulated_values[$k] = 0;
				}
		?>
        </tr>
        <?php
			}
			//Displays pagination controls
		?>
        <tr>
        	<td colspan="<?=$shown_field_count?>">
            	Page 
            	<a id="records_first"><img src="<?=$first_enabled==1 ? "/tools/view-reports/reports/phprptimages/first.gif" : "/tools/view-reports/reports/phprptimages/firstdisab.gif"?>" />
                </a>
                <a id="records_previous"><img src="<?=$previous_enabled==1 ? "/tools/view-reports/reports/phprptimages/prev.gif" : "/tools/view-reports/reports/phprptimages/prevdisab.gif"?>" /></a>
            	<input id="page_no" name="page_no" value="<?=$current_page?>" style="width:40px"<?=$attributes['form_post']['max_records'] == "all" ? '' : ""?> />
                <a id="records_next"><img src="<?=$next_enabled==1 ? "/tools/view-reports/reports/phprptimages/next.gif" : "/tools/view-reports/reports/phprptimages/nextdisab.gif"?>" /></a>
                <a id="records_last"><img src="<?=$last_enabled==1 ? "/tools/view-reports/reports/phprptimages/last.gif" : "/tools/view-reports/reports/phprptimages/lastdisab.gif"?>" /></a>
                of <?=$total_pages?>
                &nbsp;&nbsp;&nbsp;
                <?="Records ".$record_start." through ".$max_record." of ".$total_results;?>
                &nbsp;&nbsp;&nbsp;
                Results per page
                <select id="max_record_select">
                	<option value="10" <?=$attributes['form_post']['max_records'] == 10 ? "selected" : ""?>>10</option>
                	<option value="25" <?=$attributes['form_post']['max_records'] == 25 ? "selected" : ""?>>25</option>
                	<option value="50" <?=$attributes['form_post']['max_records'] == 50 ? "selected" : ""?>>50</option>
                	<option value="100" <?=$attributes['form_post']['max_records'] == 100 ? "selected" : ""?>>100</option>
                	<option value="all" <?=$attributes['form_post']['max_records'] == "all" ? "selected" : ""?>>all</option>
                </select>
                <?php //jQuery code for pagination controls?>
                <script>
					$("#page_no").keypress(function(e){
						if(e.keyCode == 13){
							$('#record_start').val(($(this).val()-1) * <?=@$_POST['max_records'] != "all" ? @$_POST['max_records'] : 0?>);
							$('#<?=$attributes['form_name']?>').submit();
						}
					});
					<?php if($first_enabled==1){?>
					$("#records_first").click(function(){
						$('#record_start').val(0);
						$('#max_records').val(<?=@$_POST['max_records']?>);
						$('#<?=$attributes['form_name']?>').submit();
					});
					<?php 
					}
					if($previous_enabled==1){?>
					$("#records_previous").click(function(){
						$('#record_start').val(<?=@$_POST['record_start'] - @$_POST['max_records']?>);
						$('#max_records').val(<?=@$_POST['max_records']?>);
						$('#<?=$attributes['form_name']?>').submit();
					});
					<?php 
					}
					if($next_enabled==1){?>
					$("#records_next").click(function(){
						$('#record_start').val(<?=$next_records?>);
						$('#max_records').val(<?=@$_POST['max_records']?>);
						$('#<?=$attributes['form_name']?>').submit();
					});
					<?php 
					}
					if($last_enabled==1){?>
					$("#records_last").click(function(){
						$('#record_start').val(<?=$last_records?>);
						$('#max_records').val(<?=@$_POST['max_records']?>);
						$('#<?=$attributes['form_name']?>').submit();
					});
					<?php 
					}
					?>
					$("#max_record_select").change(function(){
						$('#max_records').val($(this).val());
						if($(this).val() == "all"){
							$('#record_start').val(0);
						}
						$('#<?=$attributes['form_name']?>').submit();
					});
				</script>
            </td>
        </tr>
        <?php
	}
	}else{
	?>
    	<tr>
        	<td width="100%">
            	No records could be found under the specified criteria
            </td>
        </tr>
    <?php	
	}
	?>
    </table>
    <?php
	if(isset($attributes['chart_options']['data-attc-location']) && $attributes['chart_options']['data-attc-location'] != ""){
		?>
    <div id="<?=$attributes['chart_options']['data-attc-location']?>"></div>
        <?php
	}
	?>
    </div>
    <?php
}

//Formats data based on its type for display
/*
Input:
$attributes['field_type'] - Determines the proper format
$attributes['decimal_points'] - Number of decimal points to round to for numeric values
$attributes['number_format'] - Format mask for numeric values
$attributes['currency_symbol'] - Symbol to be shown for numeric values that are currencies
$attributes['multiselect_values'] - Array of multiselect values and their labels used to translate from numeric to a comma separated list

Output:
Each case returns the formatted value
*/
function format_output($value, $attributes){
	switch($attributes['field_type']){
		//Formats are grouped by type general types.  In many groups the value is merely returned and could be consolidated, but are currently left open in case this needs to be changed
			case "float":
			case "double":
			case "decimal":
			case "smallint":
			case "mediumint":
			case "bigint":
			case "int":
			case "aggregate":
				$return_value = '';
				if($value != ''){
					$decimals = 0;
					if(isset($attributes['decimal_points'])){
						$decimals = $attributes['decimal_points'];
					}
					if(isset($attributes['number_format']) && $attributes['number_format'] === false){
						$return_value = $value;
					}else{
						$return_value = number_format($value,$decimals);
					}
					if(isset($attributes['currency_symbol'])){
						$return_value = $attributes['currency_symbol'].$return_value;
					}
				}
				return $return_value;
			break;
			
			case "bit":
			case "tinyint":
				return $value;
			break;
			
			case "char":
			case "tinytext":
			case "varchar":
				return $value;
			break;
			
			case "enum":
				return $value;
			break;
			
			//Only formats date/time types if a value is returned (otherwise it will format as "12/31/1969")
			case "date":
				if($value > '0000-00-00' && $value != '' && $value != NULL){
					return date("m/d/Y",strtotime($value));
				}else{
					return '';
				}
			break;
			case "time":
				if($value > '00:00:00' && $value != '' && $value != NULL){
					return date("H:i:s", strtotime($value));
				}else{
					return '';
				}
			break;
			case "datetime":
			case "timestamp":
				if($value > '0000-00-00 00:00:00' && $value != '' && $value != NULL){
					return date("m/d/Y H:i:s A", strtotime($value));
				}else{
					return '';
				}
			break;

			
			case "longtext":
			case "mediumtext":
			case "text":
				return $value;
			break;
			case "multiselect":
				echo $value;
			break;
			case "bit_multiselect":
				$full_text = "";
				foreach($attributes['multiselect_values'] as $k => $label){
					if(intval($value) & intval($k)){
						$full_text .= $label.", ";
					}
				}
				return rtrim($full_text,", ");
			break;
	}
}

//Formats post data based on the field type for use in queries
/*
Input:
$attributes['field_type'] - Determines the proper format

Output:
Each case returns the formatted value
*/
function format_input($value, $attributes){
	switch($attributes['field_type']){
		
			case "float":
			case "double":
			case "decimal":
			case "smallint":
			case "mediumint":
			case "bigint":
			case "int":
			case "aggregate":
				return $value;
			break;
			
			case "bit":
			case "tinyint":
				return $value;
			break;
			
			case "char":
			case "tinytext":
			case "varchar":
				return $value;
			break;
			
			case "enum":
				return $value;
			break;
			
			case "time":
			case "datetime":
			case "timestamp":
				return date("Y-m-d h:i:s",strtotime($value));
			break;
			case "date":
				return date("Y-m-d",strtotime($value));
			break;
			
			case "longtext":
			case "mediumtext":
			case "text":
				return $value;
			break;
			case "multiselect":
				echo $value;
			break;
			case "bit_multiselect":
				return $value;
			break;
	}
}

//Simple function to display "a" or "an" based on if a word starts with a vowel, primarily used with error messages for missing input such as "please enter a first name."  Note: This should eventually include common phonetic exceptions.
/*
Input:
$word - The word being checked

Output:
A or an as appropriate
*/
function a_an($word){
	$vowel_array = array("A","E","I","O","U");
	if(in_array(strtoupper(substr($word, 0, 1)),$vowel_array)){
		return "an";
	}else{
		return "a";
	}
}

//Formats results so it can be exported to a file based on class.sqlreportbuilder.php functionality.

/*
Input:
$attributes['export_sql'] - SQL statement for full results set to be exported
$attributes['form_title'] - Form's title, used in this case as the filename

Output:
Creates a buffered file out of the results set in the appropriate file type with headers to force a download
*/
function export_results($attributes){
	global $pdo;
	ini_set('memory_limit', -1);
	ini_set('output_buffering', -1);
	
	$report_builder = new ReportBuilder($pdo, $attributes['export_sql']);
	$stmt = $pdo->prepare($attributes['export_sql']);
	$stmt->execute($attributes['parameters']);
	$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$now = gmdate("D, d M Y H:i:s");
	$filename = "export";
	if(isset($attributes['form_title'])){
		$filename = $attributes['form_title'].".".$_GET['fileType'];
	}
	switch($_GET['fileType']){
		case "csv":
			$now = gmdate("D, d M Y H:i:s");
			
			header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
			header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
			header("Last-Modified: {$now} GMT");
			
			//header("Last-Modified: {$now} GMT");
			
			// force download  
			header('Expires: 0');
			header('Cache-control: private');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Content-Description: File Transfer');
			header('Content-Type: text/csv');
			header('Content-disposition: attachment; filename="'.$attributes['form_title'].'.csv"');
			//header("Pragma: ");
			
			// disposition / encoding on response body
			//header("Content-Disposition: attachment;filename={$filename}");
			//header("Content-Transfer-Encoding: binary");
			$report_results = $report_builder->generateCSV($result);
			echo $report_results;
		break;
		case "pdf":
			$html = "<html>".$report_builder->generateHtml($result)."</html>";
			//echo $html;
			//exit();
			if(isset($attributes['orientation'])){
				$orientation = $attributes['orientation'];
			}else{
				$orientation = 'portrait';
			}
			$report_builder->generatePDF($html, $orientation);
		break;
	}
	exit();
}
?>